﻿using System.Collections; 
using System.Collections.Generic; 
using UnityEngine; 
using UnityEngine.UI;
using UnityEngine.SceneManagement;
 
[RequireComponent(typeof(Button))] 
public class ClickSound : MonoBehaviour { 
 
 
    public AudioClip sound;


    private Button button { get { return GetComponent<Button>(); } } 
    private AudioSource source { get { return GetComponent<AudioSource>(); } } 
  // Use this for initialization 

  void Start () { 
 
 
        gameObject.AddComponent<AudioSource>(); 
        source.clip = sound; 
        source.playOnAwake = false; 
 
        button.onClick.AddListener(() => PlaySound());
        button.onClick.AddListener(Load);
 
  } 
 
  public void PlaySound() 
    { 
        source.PlayOneShot(sound); 
    }

    private void Load()
    {
        SceneManager.LoadScene(1);
    }
} 