﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainController : MonoBehaviour {

	public bool upPress = false;
	public bool downPress = false;
	public bool leftPress = false;
	public bool rightPress = false;
    public bool space = false;
	public string debugKey;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		upPress = Input.GetButton ("Up") || Input.GetButton ("JoystickUp");
		downPress = (Input.GetButton ("Down") || Input.GetButton ("JoystickDown"));
		leftPress = (Input.GetButton ("Left") || Input.GetButton ("JoystickLeft"));
		rightPress = (Input.GetButton ("Right") || Input.GetButton ("JoystickRight"));
        space = Input.GetButton("Space");

        if (!debugKey.Equals(""))
            Debug.Log(Input.GetButtonDown(debugKey));
    }
}
