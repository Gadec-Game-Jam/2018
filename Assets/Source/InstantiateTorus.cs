﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateTorus : MonoBehaviour
{
    public GameObject torusPrefab;
    private const float zLayer = -6F;

    public void Make(float x, float y)
    {
        GameObject o = Instantiate(torusPrefab);
        o.transform.position = new Vector3(x, y, zLayer);    }
}

