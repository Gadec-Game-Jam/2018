﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimController : MonoBehaviour {

	private Animator playerAnim;
	private MainController controller;
    private Animator hatchAnim;
    private Animator mouthAnim;
    private Animator toastAnim;
    private float currentTime;
    private bool eat;
    public float openDelay;
    public float closeDelay;


    void Awake() {
		playerAnim = GetComponent<Animator> ();
		controller = GetComponent<MainController>();
        hatchAnim = GameObject.FindWithTag("hatch").GetComponent<Animator>();
        mouthAnim = GameObject.FindWithTag("mouth").GetComponent<Animator>();
        toastAnim = GameObject.FindWithTag("toast").GetComponent<Animator>();

    }

    private void Start()
    {
        currentTime = 0.5F;
        eat = false;
    }


	void Update () {
		playerAnim.SetBool ("UpPressed", controller.upPress);
		playerAnim.SetBool ("DownPressed", controller.downPress);
		playerAnim.SetBool ("LeftPressed", controller.leftPress);
		playerAnim.SetBool ("RightPressed", controller.rightPress);
        //hatchAnim.SetBool("open", controller.space);

        if (controller.space)
        {
            hatchAnim.SetBool("open", true);
            toastAnim.SetBool("eat", true);
            StartCoroutine(Cooldown(()=> eat = true, openDelay));
        }
        if (eat)
        {
            mouthAnim.SetBool("chew", true);
            StartCoroutine(Cooldown(() => eat = false, closeDelay));
        }
        else
        {
            mouthAnim.SetBool("chew", false);
            hatchAnim.SetBool("open", false);
            toastAnim.SetBool("eat", false);
        }
    }

    public IEnumerator Cooldown(System.Action condition, float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        condition();
    }
}
