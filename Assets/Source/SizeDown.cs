﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SizeDown : MonoBehaviour {
    public float startRadius = 10F;
    public float endRadius = 0.5F;
    public float rate = 0.5F;
    private Dest.Modeling.Torus torus;
    private float currentRadius;
    public bool debug;

    private void Awake()
    {
        torus = GetComponent<Dest.Modeling.Torus>();
        currentRadius = startRadius;
    }

    private void Update()
    {
        if (currentRadius > endRadius)
        {
            currentRadius -= rate;
            GetComponent<MeshFilter>().mesh = Dest.Modeling.MeshGenerator.CreateTorus(currentRadius, torus.Thickness, torus.Tessellation * 2, torus.Tessellation, torus.GenerateNormals, torus.GenerateUVs);
        }
        else
            Destroy(this.gameObject);

        if (debug)
            Debug.Log(currentRadius);
    }
}
