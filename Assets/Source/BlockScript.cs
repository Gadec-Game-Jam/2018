﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockScript : MonoBehaviour
{
	private SpriteRenderer ren;
	// Use this for initialization
	void Start () {
		ren = this.GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.anyKey)
			ren.color = Color.green;
		else
			ren.color = Color.white;
	}
}
