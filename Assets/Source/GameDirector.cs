﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using MidiSharp;
using MidiSharp.Events;
using UnityEngine;
using UnityEngine.EventSystems;

// Temporary variables sht
struct Note
{
    public uint NoteFraction;
    public bool C4;
    public bool D4;
    public bool E4;
    public bool F4;
}

struct Bar
{
    public Note[] Notes;
}

// Temporary variables sht END

public class GameDirector : MonoBehaviour
{
	/*
	 * **********concerns************
	 * Visual lag
	 * audio lag
	 * measuring visual and audio lag
	 * input is event based that logs a key press time point instead of polled in the update loop
	 * playhead position doesn't linearly update, it updates in chunks, so we need to interpolate across it
	 *
	 * ***********GameDirector************
	 * "plays" the track data
	 * 
	 * holds the
	 * 		- state of the note track,
	 * 		- state of upcoming notes
	 *

     * 
     * ***********track data****************
	 *
	 * note data structure
	 * [note(s)]
	 *
	 * a couple of colured blocks that light up when hit
	 * timers float above blocks counting down to hit time
	 * 
	 * int bars per second
	 * 
	 * note data = array of arrays
	 * [[]]
	 * main array is holds number of track "bars"
	 * [, , , , , ...]
	 *
	 * sub arrays hold notes per bar
	 *
	 * ******************test scene***************
	 * 
	 * a couple of colured blocks that light up when hit
	 * timers float above blocks counting down to hit time
	 * 
	 */


	private const string midiFileLocation = "/Sound/beepboopbeepbeep.mid";

	// Use this for initialization

	private float timeBetweenNotes = 0.5f;
	private uint barCount = 0;

	private AudioSource audsrc;
	private LevelNoteData _levelNoteData;
	
	void Start ()
	{
		//ParseMidi();
		
		audsrc = GetComponent<AudioSource>();
		
		_levelNoteData = Resources.Load<LevelNoteData>("Config/NoteTrack");
		
		// 120bpm / 60sec = 2 beats per second
		// 4 beats per measure / 2 beats per second = 2 seconds per measure
		// calculate seconds per measure
		float secondsPerMeasure = _levelNoteData.BeatsPerMeasure / (_levelNoteData.BeatsPerMinute / 60f);
		
		// 2 seconds per measure / 4 note divisor (how many note fractions per measure) = 0.5 seconds between notes
		timeBetweenNotes = secondsPerMeasure = secondsPerMeasure / _levelNoteData.NoteDivisor;
		
		barCount = (uint)_levelNoteData.Bars.Length;
		notesInCurrentBar = (uint)_levelNoteData.Bars[0].Notes.Length;

		Debug.Log($"Bar count {barCount} notesIncurrentbar {notesInCurrentBar}");
		Debug.Log($"Current bar {barPlayHead} notes in bar {notesInCurrentBar}");
	}

	private uint notePlayHead = 0;
	private uint notesInCurrentBar = 0;
	private uint barPlayHead = 0;
	private float timeSinceLastNote = 0f;
	private bool isTrackPlaying = false;

	private string notesPlayedText = "";
	private int notesPlayedCount = 0;

	private float startOffset = 0f;

	private float lastTime = 0f;

	private float predictiveOffset = 0.00f;
	// Update is called once per frame
	void Update ()
	{
		UpdateTrackPlayback();
	}

	private void UpdateTrackPlayback()
	{
		if (Input.GetKeyDown(KeyCode.P))
		{
			//reset stuff I guess
			barPlayHead = 0;
			notePlayHead = 0;

			barCount = (uint) _levelNoteData.Bars.Length;
			notesInCurrentBar = (uint) _levelNoteData.Bars[0].Notes.Length;

			notesPlayedCount = 0;
			notesPlayedText = "";

			targetNoteToHit = 1; // skip first note

			startOffset = Time.time;
			isTrackPlaying = true;
			audsrc.Play();
			notesPlayedCount++;
		}

		//go home my wayward son
		if (!isTrackPlaying) return;


		if (audsrc.time >= (timeBetweenNotes * notesPlayedCount))
		{
			//advance notes, advance bars
			if (notePlayHead == notesInCurrentBar - 1)
			{
				//30 960units 0s
				//900 960units --- 0.0625s
				//30 960units  ----- 1.9365s

				if (barPlayHead == barCount - 1)
				{
					isTrackPlaying = false;
					return;
				}

				barPlayHead++;
				notesInCurrentBar = (uint) _levelNoteData.Bars[barPlayHead].Notes.Length;
				notePlayHead = 0;
			}
			else
			{
				notePlayHead++;
			}

			//play next note

			if (notesPlayedCount % 13 == 0)
				notesPlayedText = "";

			notesPlayedCount++;
			
			lastTime = Time.time;
		}
		
		CheckBeatHit();
	}

	private uint targetNoteToHit = 0;
	private float hitTimeWindow = 0.2f; //how long until a note counts as missed after it plays
	private bool isNoteAttempted = false;
	private uint notesHit = 0;
	private uint notesMissed = 0;
	private void CheckBeatHit()
	{
		float timeTillNote = (targetNoteToHit * timeBetweenNotes) - audsrc.time;
		// attempt to hit a note
		if ((Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.S) ||
		     Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.F)) && !isNoteAttempted)
		{
			var n = _levelNoteData.Bars[barPlayHead].Notes[notePlayHead];
			bool correctKeysHit = Input.GetKeyDown(KeyCode.A) == n.C4 && Input.GetKeyDown(KeyCode.S) == n.D4 &&
			                      Input.GetKeyDown(KeyCode.D) == n.E4 && Input.GetKeyDown(KeyCode.F) == n.F4;
			
//			Debug.Log($"A {Input.GetKeyDown(KeyCode.A)} S {Input.GetKeyDown(KeyCode.S)} " +
//			          $"D {Input.GetKeyDown(KeyCode.D)} F {Input.GetKeyDown(KeyCode.F)}\n" +
//			          $"C4{n.C4}, D4{n.D4}, E4{n.E4}, F4{n.F4}");
			
			bool noteHit = (Math.Abs(timeTillNote) <= hitTimeWindow) && correctKeysHit;

			if (noteHit)
				notesHit++;
			else
				notesMissed++;
			
			isNoteAttempted = true;
		}

		//increment target note if we missed this note, or advance not
		//negative since it's after the note time of zero
		if (timeTillNote < -hitTimeWindow)
		{
			if (isNoteAttempted)
			{
				isNoteAttempted = false;
			}
//			else
//			{
//				notesPlayedText += $"TimeMiss\n";
//			}

			targetNoteToHit++;
		}
	}

	private void OnGUI()
	{
		GUI.Label(new Rect(0f, 0f, 1024f, 20f), $"Notes played: {notesPlayedCount}\t\tnotesHit: {notesHit}\t\tnotesMissed: {notesMissed}\t\tpercent: {((float)notesHit / (float)notesPlayedCount)*100:F}");
		GUI.Label(new Rect(0f, 16f,1024f, 1024f), notesPlayedText);
	}
}
