﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using MidiSharp;
using System.IO;
using System;

[CreateAssetMenu(fileName = "NoteTrack", menuName = "Custom Assets/Note Track", order = 1)]
public class LevelNoteData : ScriptableObject
{
    Note_Time getLastNote(List<Note_Time> bar)
    {
        Note_Time note = new Note_Time();
        note.C4 = bar[bar.Count - 1].C4;
        note.D4 = bar[bar.Count - 1].D4;
        note.E4 = bar[bar.Count - 1].E4;
        note.F4 = bar[bar.Count - 1].F4;
        return note;
    }

    Note_Time createNote()
    {
        Note_Time newNote = new Note_Time();
        newNote.C4 = false;
        newNote.D4 = false;
        newNote.E4 = false;
        newNote.F4 = false;
        return newNote;
    }

    Note createRealNote()
    {
        Note newNote = new Note();
        newNote.C4 = false;
        newNote.D4 = false;
        newNote.E4 = false;
        newNote.F4 = false;
        newNote.NoteFraction = 4; //Set this to something legit later when you implement > quarter notes
        return newNote;
    }

    // Edits a given note given the right parameters
    // If deltaTime shouldn't be updated, set it to 0.
    Note_Time editNote(Note_Time note, string noteNumber, long deltaTime, int division)
    {
        if ("C4".Equals(noteNumber))
        {
            note.C4 = true;
        }
        else if ("D4".Equals(noteNumber))
        {
            note.D4 = true;
        }
        else if ("E4".Equals(noteNumber))
        {
            note.E4 = true;
        }
        else if ("F4".Equals(noteNumber))
        {
            note.F4 = true;
        }
        if (deltaTime != 0)
        {
            note.deltaTime = deltaTime;
        }
        return note;
    }

    public AudioClip AudioTrack;
	
	public uint BeatsPerMinute = 120;
	public uint BeatsPerMeasure = 4;
	public uint NoteDivisor = 4;

    [System.Serializable]
    public struct Note_Time
    {
        public long deltaTime;
        public bool C4;
        public bool D4;
        public bool E4;
        public bool F4;
    }

    [System.Serializable]
	public struct Note
	{
		public uint NoteFraction;
		public bool C4;
		public bool D4;
		public bool E4;
		public bool F4;
	}

	[System.Serializable]
	public struct Bar
	{
		public Note[] Notes;
	}

	public Bar[] Bars;

    public void Init(string midiFileLocation)
    {
        //Fill in notes and bars here
        //Don't delete this :(
        MidiSequence sequence;

        using (Stream inputStream = File.OpenRead(Application.dataPath + midiFileLocation))
        {
            sequence = MidiSequence.Open(inputStream);
            Debug.Log(sequence);
        }
        //Don't delete this END

        MidiEventCollection events = sequence.Tracks[sequence.Tracks.Count - 1].Events;
        int division = sequence.Division;

        // Setup variables for iterative note and bar creation
        List<Note_Time> recentBar = new List<Note_Time>(); // Not recent anymore :) - check past implementation for segregating bars
        Note_Time garbageNote; // Used for struct editing
        Note_Time[] garbageNoteArray;
        long barTime = 0;
        string[] eventString;

        // Assumes midi has at least one note, first note is a special case since deltaTime is 0
        // Find the first note and initiate the first bar
        int counter = 0;
        for (int i = 0; i < events.Count; i++)
        {
            eventString = events[i].ToString().Split();
            if ("OnNoteVoiceMidiEvent".Equals(eventString[0]) || "OffNoteVoiceMidiEvent".Equals(eventString[0]))
            {
                //Setup new note
                garbageNote = new Note_Time();
                garbageNote = editNote(garbageNote, eventString[3], 0, division);
                recentBar.Add(garbageNote);
                counter = i + 1;
                break;
            }
        }

        // Reads collection of strings and puts them into note struct
        for (int i = counter; i < events.Count; i++)
        {
            eventString = events[i].ToString().Split();

            //Normal case: Event is an OnNoteVoiceMidiEvent or OffNoteVoiceMidiEvent
            if ("OnNoteVoiceMidiEvent".Equals(eventString[0]) || "OffNoteVoiceMidiEvent".Equals(eventString[0]))
            {
                Debug.Log(events[i].DeltaTime);
                Debug.Log(eventString[0] + "," + eventString[1] + "," + eventString[2] + "," + eventString[3]);

                //Set the NoteFraction of the previous note using the delta time
                //If it completes a bar, make the bar (unless this note is an OffNoteVoiceMidiEvent with deltaTime = 0)

                //Five possible cases:
                //1) OnNoteVoiceMidiEvent - its a note
                //2) OnNoteVoiceMidiEvent with a deltatime at the end of a beginning/end of a bar <-- screw this case
                //3) OnNoteVoiceMidiEvent with a deltatime = 0 - it's at the same time as the last note
                //4) OffNoteVoiceMidiEvent with a deltatime - it's an offnote
                //5) OffNoteVoiceMidiEvent with a deltatime = 0 - there's a consecutive on note right after the off note 
                //TODO: Inquire if we still even need 'bars'
                //      Take care of case where deltaTime = 0 and consecutiveOffBeat is right after

                barTime += events[i].DeltaTime; // Add up bar time

                if ("OnNoteVoiceMidiEvent".Equals(eventString[0]))
                {
                    if (events[i].DeltaTime == 0 && i == 2) //Case 3 - make sure is not the first note
                    {
                        //Edit previous note's melody
                        garbageNote = getLastNote(recentBar);
                        garbageNote = editNote(garbageNote, eventString[3], 0, division);
                        recentBar[recentBar.Count - 1] = garbageNote;
                    }
                    else
                    {
                        //Case 1
                        //Set delta time of previous note
                        garbageNote = getLastNote(recentBar);
                        garbageNote = editNote(garbageNote, null, events[i].DeltaTime, division);
                        recentBar[recentBar.Count - 1] = garbageNote;

                        //Setup new note
                        garbageNote = createNote();
                        garbageNote = editNote(garbageNote, eventString[3], 0, division);
                        recentBar.Add(garbageNote);
                    }
                }
                else //OffNoteVoiceMidiEvent
                {
                    if (events[i].DeltaTime == 0) // case 5
                    {
                        //Set delta time for last note
                        garbageNote = getLastNote(recentBar);
                        garbageNote = editNote(garbageNote, null, events[i].DeltaTime, division);
                        recentBar[recentBar.Count - 1] = garbageNote;
                    }
                    else // case 4
                    {
                        //Set delta time for last note
                        garbageNote = getLastNote(recentBar);
                        garbageNote = editNote(garbageNote, null, events[i].DeltaTime, division);
                        recentBar[recentBar.Count - 1] = garbageNote;

                        //Setup off note 
                        recentBar.Add(createNote());
                    }
                }

                //Add deltaTime to barTime
            }
            else if ("ControllerVoiceMidiEvent".Equals(eventString[0]))
            {
                //Conclusive case: Reached last midi event, is either ControllerVoiceMidiEvent or EndOfTrackMetaMidiEvent 
                //Debug.Log(events[i].DeltaTime);
                //Debug.Log(eventString[0] + "," + eventString[1] + "," + eventString[2] + "," + eventString[3]);
                //Edit last note seen
                garbageNote = getLastNote(recentBar);
                garbageNote = editNote(garbageNote, null, events[i].DeltaTime, division);
                recentBar[recentBar.Count - 1] = garbageNote;

                //Put recentBar into totalBars
                /*
                garbageNoteArray = new Note_Time[recentBar.Count];
                for (int k = 0; i < garbageNoteArray.Length; k++)
                {
                    garbageNoteArray[k] = recentBar[k];
                }

                garbageBar = new Bar();
                garbageBar.Notes = garbageNoteArray;
                totalBars.Add(garbageBar);
                */
                break;
            }
        }

        //Put everything in recentBar into a list of notes
        //Essentially, turn List<Note_Time> into List<Note>
        //Assume everything is in quarter notes
        int quarterDivision = division / 4;
        List<Note> finalNoteList = new List<Note>();
        for (int i = 0; i < recentBar.Count; i++) //Divide all note_time that have a deltaTime > quarterDivision
        {
            long amountToCreate = recentBar[i].deltaTime / quarterDivision;
            while (amountToCreate > 0)
            {
                Note newNote = new Note();
                newNote.C4 = recentBar[i].C4;
                newNote.D4 = recentBar[i].D4;
                newNote.E4 = recentBar[i].E4;
                newNote.F4 = recentBar[i].F4;
                newNote.NoteFraction = (uint) (division / quarterDivision);
                finalNoteList.Add(newNote);
                amountToCreate = amountToCreate - 1;
            }
        }

        Debug.Log(finalNoteList);

        //Using finalNoteList (i.e. List<Note>), create bars of 4 notes each
        //Special case: If last note ends somewhere before the 4th note, put in empty notes
        int noteCounter = 0;
        int sizeOfBars;
        if(finalNoteList.Count % 4 == 0)
        {
            sizeOfBars = finalNoteList.Count / 4;
        } else
        {
            sizeOfBars = finalNoteList.Count / 4 + 1;
        }
        Note[] garbageBar = new Note[4];
        Bars = new Bar[sizeOfBars];
        sizeOfBars = 0; //Used to count how far we are in the bar land
        Bar someBarName;
        for (int i = 0; i < finalNoteList.Count; i++)
        {
            garbageBar[noteCounter] = finalNoteList[i];
            noteCounter++;
            if(i == finalNoteList.Count - 1) //Special case: end of finalNoteList
            {
                for(int k = 0; k < 3 - noteCounter + 1; k++)
                {
                    garbageBar[k] = createRealNote();
                }
                someBarName = new Bar();
                someBarName.Notes = garbageBar;
                Bars[sizeOfBars] = someBarName;
                sizeOfBars++;
                noteCounter = 0;
            } else if (noteCounter == 4) //End of a bar
            {
                someBarName = new Bar();
                someBarName.Notes = garbageBar;
                Bars[sizeOfBars] = someBarName;
                sizeOfBars++;
                noteCounter = 0;
            }
        }
    }

}


