﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateLevel : MonoBehaviour {

    LevelNoteData levelInstance;

	// Use this for initialization
	void Start () {
        levelInstance = LevelNoteData.CreateInstance<LevelNoteData>();
        levelInstance.Init("/Sound/beepboopbeepbeep.mid");
        for(int i = 0; i < levelInstance.Bars.Length; i++)
        {
            for(int k = 0; k < levelInstance.Bars[i].Notes.Length; k++)
            {
                LevelNoteData.Note printNote = levelInstance.Bars[i].Notes[k];
                Debug.Log("Bar "+i+": Playing "+ printNote.C4 + " " + printNote.D4 + " " + printNote.E4 + " " + printNote.F4 + " " + printNote.NoteFraction);
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
