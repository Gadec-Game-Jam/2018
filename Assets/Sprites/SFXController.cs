﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXController : MonoBehaviour {

	public AudioClip clip;

	// Valid values are between 0 and 1
	public float vol;

	private AudioSource source;

	void Awake () {
		source = GetComponent<AudioSource> ();
	}

	void PlaySFX () {
		source.PlayOneShot (clip, vol);
	}
}
